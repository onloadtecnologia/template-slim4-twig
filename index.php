<?php

use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;


require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/controller/HomeController.php';

// Create App
$app = AppFactory::create();

// Create Twig
$twig = Twig::create('templates', ['cache' => false]);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/', HomeController::class . ":home")->setName('home');
$app->get('/sobre', HomeController::class . ":sobre")->setName('sobre');

// Run app
$app->run();
